type Task = (...dependencyResults: any[]) => any;

export interface TaskDict {
    [taskId: string]: {
        dependencies: string[]; // an array of task ids.
        task: Task;
    };
}
export interface TaskResultDict {
    [taskId: string]:
        | {
              status: 'resolved';
              value: any;
          }
        | {
              status: 'failed';
              reason: any;
          }
        | {
              status: 'skipped';
              unresolvedDependencies: string[];
          };
}

interface InterimResultDict {
    todo: string[];
    done: TaskResultDict;
}

interface DependenciesRoutes {
    [taskId: string]: string[];
}

interface TaskResultValueDict {
    status: 'resolved' | 'failed';
    value?: any;
    reason?: any;
}

interface SettledPromiseResult {
    status: 'fulfilled' | 'rejected';
    value?: any;
    reason?: any;
}

export async function runTasksAsync(tasks: TaskDict): Promise<TaskResultDict> {
    const circTasks = findCircTasks(tasks);
    const initTodo = removeArrayItems(Object.keys(tasks), Object.keys(circTasks));
    let init = { todo: initTodo, done: circTasks };
    let { todo, done } = await runTasksRound(tasks, init);
    while (todo.length) {
        const result = await runTasksRound(tasks, { todo, done });
        todo = result.todo;
        done = result.done;
    }
    return done;
}

async function runTasksRound(tasks: TaskDict, result: InterimResultDict) {
    result = await runReadyTasks(tasks, result);
    return handleFailed(tasks, result);
}

async function runReadyTasks(tasks: TaskDict, result: InterimResultDict) {
    const readyTaskKeys = findReadyTasks(tasks, result);
    const readyTaskPromises = readyTaskKeys.map((key) => {
        const { dependencies, task } = tasks[key];
        const args = getDependenciesValues(dependencies, result);
        return promisify(task, args);
    });
    const settled = await Promise.allSettled(readyTaskPromises);
    const settledTasks = settled
        .map((val, i) => convertToResult(val, readyTaskKeys[i]))
        .reduce((acc, resObj: TaskResultDict) => ({ ...acc, ...resObj }), {});
    result.done = { ...result.done, ...settledTasks };
    return updateTodo(result);
}

function handleFailed(tasks: TaskDict, result: InterimResultDict) {
    for (const name of result.todo) {
        const { dependencies } = tasks[name];
        if (dependencies.every((depName) => result.done[depName])) {
            const failedDeps = dependencies.filter((depName) => {
                const depItem = result.done[depName];
                return depItem.status === 'skipped' || depItem.status === 'failed';
            });
            if (failedDeps.length) {
                result.done[name] = {
                    status: 'skipped',
                    unresolvedDependencies: failedDeps,
                };
            }
        }
    }
    return updateTodo(result);
}

function updateTodo(result: InterimResultDict): InterimResultDict {
    const doneTasks = Object.keys(result.done);
    result.todo = removeArrayItems(result.todo, doneTasks);
    return result;
}

function findReadyTasks(tasks: TaskDict, result: InterimResultDict): string[] {
    return result.todo.reduce((acc: string[], name: string) => {
        const { dependencies } = tasks[name];
        if (isDependenciesResolved(dependencies, result)) {
            acc.push(name);
        }
        return acc;
    }, []);
}

function findCircTasks(tasks: TaskDict): TaskResultDict {
    const names = Object.keys(tasks);
    return names.reduce((acc: TaskResultDict, name: string) => {
        const circRoute = findCircRoute(tasks, name);
        if (circRoute) {
            acc[name] = {
                status: 'skipped',
                unresolvedDependencies: [circRoute],
            };
        }
        return acc;
    }, {});
}

function isDependenciesResolved(dependencies: string[], result: InterimResultDict) {
    if (dependencies.length === 0) {
        return true;
    }
    return dependencies.every((depName) => {
        return result.done[depName] && result.done[depName].status === 'resolved';
    });
}

function getDependenciesValues(dependencies: string[], result: InterimResultDict) {
    if (dependencies.length === 0) {
        return [];
    }
    return dependencies.map((depName) => {
        return (<TaskResultValueDict>result.done[depName]).value;
    });
}

function findCircRoute(tasks: TaskDict, rootKey: string): string | undefined {
    const { dependencies } = tasks[rootKey];
    if (dependencies.length === 0) {
        return;
    }
    const routes = dependencies.reduce((acc, s) => {
        acc[s] = findDependenciesRoutes(tasks, s);
        return acc;
    }, {} as DependenciesRoutes);
    return Object.keys(routes).find((key) => routes[key].includes(rootKey));
}

function findDependenciesRoutes(tasks: TaskDict, key: string, result: string[] = []): string[] {
    const { dependencies } = tasks[key];
    let newPaths: string[] = [];
    if (dependencies.length) {
        newPaths = dependencies.filter((s) => !result.includes(s));
    }
    if (!newPaths.length) {
        return result;
    }
    result = result.concat(newPaths);
    return newPaths.reduce((acc, s) => {
        return findDependenciesRoutes(tasks, s, acc);
    }, result);
}

function convertToResult(promiseResult: SettledPromiseResult, taskID: string): TaskResultDict {
    const { status, value, reason } = promiseResult;
    if (status === 'rejected') {
        return {
            [taskID]: {
                status: 'failed',
                reason,
            },
        };
    }
    return {
        [taskID]: {
            status: 'resolved',
            value,
        },
    };
}

function promisify(task: Task, args: any[]): Promise<any> {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await task(...args);
            resolve(result);
        } catch (e) {
            reject(e);
        }
    });
}

function removeArrayItems(array: any[], dropItems: any[]) {
    return array.filter((n) => !dropItems.includes(n));
}
