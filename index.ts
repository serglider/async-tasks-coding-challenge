import { runTasksAsync, TaskResultDict, TaskDict } from './solution';

export const runTasks = (tasks: TaskDict): Promise<TaskResultDict> => {
    return runTasksAsync(tasks);
};
